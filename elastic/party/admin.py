# -*- coding: utf-8 -*-

from django.contrib import admin
from models import *
from forms import PartyForm

__author__ = "Igor S. Kovalenko"
__contact__ = "kovalenko@sb-soft.biz"
__site__ = "http://www.elastic-trade-server.org"
__year__ = "2015"
__description__ = "Party package"


class LinkChannelInline(admin.TabularInline):
    model = LinkChannel
    fieldsets = (
        (None, {
            'fields':  ['name', 'value', 'is_verified', ]
        }),
    )


class HolidayInline(admin.TabularInline):
    model = Holiday


class PhotoInline(admin.TabularInline):
    model = Photo


class PartyAdmin(admin.ModelAdmin):
    list_display = ('name', 'party_type', )
    inlines = [
        HolidayInline,
        LinkChannelInline,
        PhotoInline,
    ]
    form = PartyForm

    search_fields = ('name', 'link_channels__value',)

    fieldsets = (
        (None, {
            'fields':  ['name', 'first_name', 'middle_name', 'last_name', 'gender', 'marital_status', 'addresses']
        }),
    )

admin.site.register(Party, PartyAdmin)
