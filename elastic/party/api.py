# -*- coding: utf-8 -*-

from models import *
from tastypie import fields
from tastypie.resources import ModelResource
from tastypie.constants import ALL, ALL_WITH_RELATIONS
from tastypie.authentication import SessionAuthentication, BasicAuthentication, MultiAuthentication
from django.utils.translation import ugettext_lazy as _
from tastypie.authorization import Authorization


__author__ = "Igor S. Kovalenko"
__contact__ = "kovalenko@sb-soft.biz"
__site__ = "http://www.elastic-trade-server.org"
__year__ = "2015"
__description__ = "Party package"


class LinkChannelResource(ModelResource):
    class Meta:
        queryset = LinkChannel.objects.all()
        resource_name = 'link_channel'
        authentication = MultiAuthentication(SessionAuthentication(), BasicAuthentication())
        authorization = Authorization()
        excludes = ['id', ]
        include_resource_uri = False
        filtering = {
            "name": ALL,
            "value": ALL,
            "party": ALL_WITH_RELATIONS,
        }

    name = fields.CharField(attribute='name', help_text=_('Channel type'))
    value = fields.CharField(attribute='value', help_text=_('Value'))
    is_verified = fields.BooleanField(attribute='is_verified', readonly=True, help_text='Is verified')
    party = fields.ToOneField('elastic.party.api.PartyResource', attribute='party', full=False)

    def dehydrate(self, bundle):
        bundle.data = dict(filter(lambda item: item[1] is not None, bundle.data.iteritems()))
        bundle.data.pop('party')
        return bundle


class HolidayResource(ModelResource):
    class Meta:
        queryset = Holiday.objects.all()
        resource_name = 'holiday'
        authentication = MultiAuthentication(SessionAuthentication(), BasicAuthentication())
        authorization = Authorization()
        excludes = ['id', ]
        include_resource_uri = False
        filtering = {
            "name": ALL,
            "holiday": ALL,
            "party": ALL_WITH_RELATIONS,
        }

    name = fields.CharField(attribute='name', help_text=_('Channel type'))
    holiday = fields.DateField(attribute='holiday', help_text=_('Holiday'))
    party = fields.ToOneField('elastic.party.api.PartyResource', attribute='party', full=False)

    def dehydrate(self, bundle):
        bundle.data = dict(filter(lambda item: item[1] is not None, bundle.data.iteritems()))
        bundle.data.pop('party')
        return bundle


class PartyResource(ModelResource):
    class Meta:
        queryset = Party.objects.all()
        resource_name = 'party'
        authentication = MultiAuthentication(SessionAuthentication(), BasicAuthentication())
        authorization = Authorization()
        excludes = ['id', ]
        include_resource_uri = False
        filtering = {
            "id": ALL,
            "name": ALL,
            "link_channels": ALL_WITH_RELATIONS,
            "holidays": ALL_WITH_RELATIONS,
        }

    name = fields.CharField(attribute='name', help_text=_('Name'))
    first_name = fields.CharField(attribute='first_name', null=True, blank=True, help_text=_('First name'))
    last_name = fields.CharField(attribute='last_name', null=True, blank=True, help_text=_('Last name'))
    middle_name = fields.CharField(attribute='middle_name', null=True, blank=True, help_text=_('Middle name'))
    addresses = fields.ToManyField(to='elastic.address.api.AddressResource', attribute='addresses',
                                   related_name='party', full=True, null=True, blank=True, help_text=_('Addresses'))
    link_channels = fields.ToManyField(LinkChannelResource, 'link_channels', related_name='party',
                                       full=True, null=True, blank=True, help_text=_('Link channels'))
    holidays = fields.ToManyField(HolidayResource, 'holidays', related_name='party',
                                  full=True, null=True, blank=True, help_text=_('Holidays'))

    def hydrate(self, bundle):
        bundle.obj.link_channels.all().delete()
        bundle.obj.holidays.all().delete()
        bundle.obj.photos.all().delete()
        return bundle

    def dehydrate(self, bundle):
        bundle.data = dict(filter(lambda item: item[1] is not None, bundle.data.iteritems()))
        return bundle
