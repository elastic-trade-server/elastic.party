# -*- coding: utf-8 -*-

import autocomplete_light.shortcuts as autocomplete_light
from models import *

__author__ = "Igor S. Kovalenko"
__contact__ = "kovalenko@sb-soft.biz"
__site__ = "http://www.elastic-trade-server.org"
__year__ = "2015"
__description__ = "Party package"


autocomplete_light.register(
    Address,
    search_fields=['city', 'street', ],
    attrs={
        'placeholder': 'Адрес....',
        'data-autocomplete-minimum-characters': 2,
    },
    widget_attrs={
        'data-widget-maximum-values': 4,
        'class': 'modern-style',
    },
)
