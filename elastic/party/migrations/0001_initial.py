# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import filer.fields.image


class Migration(migrations.Migration):

    dependencies = [
        ('filer', '0002_auto_20150606_2003'),
        ('address', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Holiday',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=256, verbose_name='Holiday name')),
                ('holiday', models.DateField(verbose_name='Holiday')),
            ],
            options={
                'verbose_name': 'Holiday',
                'verbose_name_plural': 'Holidays',
            },
        ),
        migrations.CreateModel(
            name='LinkChannel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=56, verbose_name='Channel type', choices=[(b'Phone', 'Phone'), (b'Mobile', 'Mobile'), (b'Fax', 'Fax'), (b'E-Mail', 'E-Mail'), (b'ICQ', 'ICQ'), (b'Mail-Agent', 'Mail-Agent'), (b'Jabber', 'Jabber'), (b'Website', 'Website'), (b'Skype', 'Skype'), (b'SIP', 'SIP'), (b'IRC', 'IRC'), (b'Other', 'Other')])),
                ('value', models.CharField(max_length=255, verbose_name='Value')),
                ('is_verified', models.BooleanField(default=False, verbose_name='Is verified')),
            ],
            options={
                'verbose_name': 'Link Channel',
                'verbose_name_plural': 'Link Channels',
            },
        ),
        migrations.CreateModel(
            name='Party',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(db_index=True, max_length=255, null=True, verbose_name='Party name', blank=True)),
                ('last_name', models.CharField(max_length=128, null=True, verbose_name='Person last name', blank=True)),
                ('middle_name', models.CharField(max_length=128, null=True, verbose_name='Person middle name', blank=True)),
                ('first_name', models.CharField(max_length=128, null=True, verbose_name='Person first name', blank=True)),
                ('marital_status', models.CharField(default=b'undefined', choices=[(b'single', 'Single'), (b'married', 'Married'), (b'widow', 'Widow'), (b'undefined', 'Undefined')], max_length=16, blank=True, null=True, verbose_name='Marital status')),
                ('gender', models.CharField(default=b'undefined', choices=[(b'male', 'Male'), (b'female', 'Female'), (b'undefined', 'Undefined')], max_length=16, blank=True, null=True, verbose_name='Gender')),
                ('addresses', models.ManyToManyField(to='address.Address', verbose_name='Addresses list')),
            ],
            options={
                'verbose_name': 'Party',
                'verbose_name_plural': 'Parties',
            },
        ),
        migrations.CreateModel(
            name='Photo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=256, verbose_name='Title')),
                ('party', models.ForeignKey(related_name='photos', verbose_name='Party', to='party.Party')),
                ('photo', filer.fields.image.FilerImageField(related_name='party_photos', verbose_name='Photo', blank=True, to='filer.Image', null=True)),
            ],
        ),
        migrations.AddField(
            model_name='linkchannel',
            name='party',
            field=models.ForeignKey(related_name='link_channels', verbose_name='Party', to='party.Party'),
        ),
        migrations.AddField(
            model_name='holiday',
            name='party',
            field=models.ForeignKey(related_name='holidays', verbose_name='Party', to='party.Party'),
        ),
    ]
