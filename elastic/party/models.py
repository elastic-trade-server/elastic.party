# -*- coding: utf-8 -*-

from django.db import models
from django.utils.translation import ugettext_lazy as _
from elastic.address.models import Address
from filer.fields.image import FilerImageField
from django.conf import settings
from django.utils import translation

__author__ = "Igor S. Kovalenko"
__contact__ = "kovalenko@sb-soft.biz"
__site__ = "http://www.elastic-trade-server.org"
__year__ = "2015"
__description__ = "Party package"


class Party(models.Model):
    """
    Особа (персона, субъект)
    Класс "Особа" инкапсулирует данные о наименовании субъекта, его почтовом адресе и каналах связи c ним.
    Если особа - физическое лицо, то прибавляются данные об имени, фамилии и отчестве субъекта,
    сведения о поле и семейном положении, а так же и набор  личных праздников субъекта
    """
    class Meta:
        verbose_name = _('Party')
        verbose_name_plural = _('Parties')

    MARITAL_STATUS_TYPE = (
        ('single', _('Single')),  # Не женат
        ('married', _('Married')),  # Женат/За мужем
        ('widow', _('Widow')),  # Вдовец/вдова
        ('undefined', _('Undefined'))  # Нет сведений
    )

    GENDER_TYPE = (
        ('male', _('Male')),  # Мужской пол
        ('female', _('Female')),  # Женский пол
        ('undefined', _('Undefined'))  # Нет сведений
    )

    #: Наименование
    name = models.CharField(null=True, blank=True, verbose_name=_('Party name'), max_length=255, db_index=True)

    #: Фамилия
    last_name = models.CharField(verbose_name=_('Person last name'), max_length=128, null=True, blank=True)

    #: Отчество
    middle_name = models.CharField(verbose_name=_('Person middle name'), max_length=128, null=True, blank=True)

    #: Имя
    first_name = models.CharField(verbose_name=_('Person first name'), max_length=128, null=True, blank=True)

    #: Коллекция адресов
    addresses = models.ManyToManyField(Address, verbose_name=_('Addresses list'))

    #: Семейное положение
    marital_status = models.CharField(null=True, blank=True, max_length=16, choices=MARITAL_STATUS_TYPE,
                                      default='undefined', verbose_name=_('Marital status'))

    #: Пол
    gender = models.CharField(null=True, blank=True, max_length=16, choices=GENDER_TYPE, default='undefined',
                              verbose_name=_('Gender'))

    def party_type(self):
        if self.first_name and self.last_name:
            return _('Person')
        else:
            return _('Legal')
    party_type.short_description = _('Party type')

    def __unicode__(self):
        return unicode(self.name)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if not self.id and self.first_name and self.last_name:
            middle_name = self.middle_name if self.middle_name else ''
            self.name = u"{0} {1} {2}".format(self.last_name, self.first_name, middle_name)
        super(Party, self).save(force_insert, force_update, using, update_fields)


class LinkChannel(models.Model):
    """
    Канал связи
    """
    class Meta:
        verbose_name = _('Link Channel')
        verbose_name_plural = _('Link Channels')

    CHANNEL_TYPE = (
        ('Phone', _('Phone')),
        ('Mobile', _('Mobile')),
        ('Fax', _('Fax')),
        ('E-Mail', _('E-Mail')),
        ('ICQ', _('ICQ')),
        ('Mail-Agent', _('Mail-Agent')),
        ('Jabber', _('Jabber')),
        ('Website', _('Website')),
        ('Skype', _('Skype')),
        ('SIP', _('SIP')),
        ('IRC', _('IRC')),
        ('Other', _('Other')),
    )

    #: Ключ
    name = models.CharField(verbose_name=_('Channel type'), max_length=56, choices=CHANNEL_TYPE)

    #: Значение
    value = models.CharField(verbose_name=_('Value'), max_length=255)

    #: Канал связи проверен
    is_verified = models.BooleanField(default=False, verbose_name=_('Is verified'))

    #: Ссылка на родительский справочник
    party = models.ForeignKey(Party, related_name='link_channels', verbose_name=_('Party'))

    def __unicode__(self):
        return u"{0}: {1}".format(self.name, self.value)

    def get_verbose_name(self):
        translation.activate(settings.LANGUAGE_CODE)
        for t in self.CHANNEL_TYPE:
            if t[0] == self.name:
                return u"".join(t[1])
        return None


class Holiday(models.Model):
    """
    Праздник
    """
    class Meta:
        verbose_name = _('Holiday')
        verbose_name_plural = _('Holidays')

    #: Наименование
    name = models.CharField(verbose_name=_('Holiday name'), max_length=256)

    #: Дата праздника
    holiday = models.DateField(verbose_name=_('Holiday'))

    #: Ссылка на родительский справочник
    party = models.ForeignKey(Party, related_name='holidays', verbose_name=_('Party'))

    def __unicode__(self):
        return unicode(self.name)


class Photo(models.Model):
    """
    Фотография
    """

    #: Заголовок
    title = models.CharField(verbose_name=_('Title'), max_length=256)

    #: Фотография
    photo = FilerImageField(null=True, blank=True, related_name='party_photos', verbose_name=_('Photo'))

    #: Ссылка на родительский справочник
    party = models.ForeignKey(Party, related_name='photos', verbose_name=_('Party'))
