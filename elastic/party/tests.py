# -*- coding: utf-8 -*-

#
# Copyright (c) 2013 SB-Soft Ltd
#

__author__ = "SB-Soft Ltd"
__contact__ = "support@sb-soft.biz"
__company__ = "SB-Soft"
__year__ = "2013"
__version__ = "0.1"
__description__ = "Party module"


from tastypie.test import ResourceTestCase
from esb.party.models import *
from django.contrib.auth.models import User


class PersonResourceTest(ResourceTestCase):
    def setUp(self):
        super(PersonResourceTest, self).setUp()

        self.cred = {
            'user': {'username': 'ivan.fomichev', 'password': '12345789', 'email': 'fomka@mail.ru', },
            'superuser': {'username': 'igor.kovalenko', 'password': 'j0ker', 'email': 'kovalenko@sb-soft.biz', },
        }
        User.objects.create_user(
            self.cred['user']['username'], self.cred['user']['email'], self.cred['user']['password'])
        su = User.objects.create_user(
            self.cred['superuser']['username'], self.cred['superuser']['email'], self.cred['superuser']['password'])
        su.is_superuser = True
        su.save()

        self.record = Person.objects.create(first_name=u'Игорь', middle_name=u'Станиславович', last_name=u'Коваленко',
                                            tax_no=12345678901)

    def get_user_credentials(self):
        return self.create_basic(username=self.cred['user']['username'], password=self.cred['user']['password'])

    def get_superuser_credentials(self):
        return self.create_basic(username=self.cred['superuser']['username'],
                                 password=self.cred['superuser']['password'])

    ##### GET tests
    def test_get_unauthorized(self):
        self.assertHttpUnauthorized(self.api_client.get("/api/v1/party/person/", format='json'))

    def test_get_detail_unauthorized(self):
        self.assertHttpUnauthorized(self.api_client.get("/api/v1/party/person/{0}/".format(self.record.id),
                                                        format='json'))

    def test_get_list_json(self):
        resp = self.api_client.get('/api/v1/party/person/', format='json',
                                   authentication=self.get_superuser_credentials())
        self.assertValidJSONResponse(resp)

        self.assertEqual(len(self.deserialize(resp)['objects']), 1)

        record = {
            'party': [],
            'first_name': u"Игорь",
            'id': self.record.id,
            'last_name': u"Коваленко",
            'middle_name': u"Станиславович",
            'tax_no': 12345678901,
            'resource_uri': "/api/v1/party/person/{0}/".format(self.record.tax_no),
        }
        self.assertEqual(self.deserialize(resp)['objects'].count(record), 1)

    def test_get_detail_json(self):
        resp = self.api_client.get("/api/v1/party/person/{0}/".format(self.record.tax_no), format='json',
                                   authentication=self.get_superuser_credentials())
        self.assertValidJSONResponse(resp)

        self.assertKeys(self.deserialize(resp), ['id', 'first_name', 'last_name', 'middle_name', 'tax_no', 'party',
                                                 'resource_uri', ])
        self.assertEqual(self.deserialize(resp)['id'], self.record.id)

    ##### POST tests
    def test_post_unauthenticated(self):
        self.assertHttpUnauthorized(self.api_client.post("/api/v1/party/person/"))

    def test_post_detail_unauthenticated(self):
        self.assertHttpUnauthorized(self.api_client.post("/api/v1/party/person/{0}/".format(self.record.tax_no)))

    def test_post_json(self):
        record = {'first_name': u"Иван", 'last_name': u"Лохов", 'middle_name': u"Петрович", 'tax_no': 12354678901}

        self.assertHttpCreated(self.api_client.post("/api/v1/party/person/", data=record, format='json',
                                                    authentication=self.get_superuser_credentials()))
        self.assertEqual(len(Person.objects.filter(last_name=u"Лохов")), 1)

    def test_post_detail_json(self):
        self.assertHttpNotImplemented(self.api_client.post("/api/v1/party/person/{0}/".format(self.record.tax_no),
                                      authentication=self.get_superuser_credentials()))

    ##### PUT tests
    def test_put_unauthenticated(self):
        self.assertHttpUnauthorized(self.api_client.put("/api/v1/party/person/".format(self.record.tax_no)))

    def test_put_detail_unauthenticated(self):
        self.assertHttpUnauthorized(self.api_client.put("/api/v1/party/person/{0}/".format(self.record.tax_no)))

    def test_put_json(self):
        self.assertHttpBadRequest(self.api_client.put("/api/v1/party/person/",
                                                      authentication=self.get_superuser_credentials()))

    def test_put_detail_json(self):
        record = Person.objects.create(first_name=u'Иван', middle_name=u'Петрович', last_name=u'Лохов',
                                       tax_no=12346578901)
        new_record = {'first_name': u"Иван", 'last_name': u"Раскольников", 'middle_name': u"Петрович",
                      'tax_no': 12346578901}

        self.assertHttpAccepted(self.api_client.put("/api/v1/party/person/{0}/".format(record.tax_no),
                                                    data=new_record, format='json',
                                                    authentication=self.get_superuser_credentials()))
        self.assertEqual(len(Person.objects.filter(last_name=u"Раскольников")), 1)

    ##### PATCH tests
    def test_patch_unauthenticated_json(self):
        self.assertHttpUnauthorized(self.api_client.patch("/api/v1/party/person/", data={}))

    def test_patch_detail_unauthenticated_json(self):
        self.assertHttpUnauthorized(self.api_client.patch("/api/v1/party/person/{0}/".format(self.record.tax_no),
                                                          data={}))

    def test_patch_json(self):
        self.assertHttpBadRequest(self.api_client.put("/api/v1/party/person/", data={},
                                                      authentication=self.get_superuser_credentials()))

    def test_patch_detail_json(self):
        record = Person.objects.create(first_name=u'Иван', middle_name=u'Петрович', last_name=u'Лохов',
                                       tax_no=12346578901)
        new_record = {'last_name': u"Раскольников", }

        self.assertHttpAccepted(self.api_client.patch("/api/v1/party/person/{0}/".format(record.tax_no),
                                                      data=new_record, format='json',
                                                      authentication=self.get_superuser_credentials()))
        self.assertEqual(Person.objects.get(last_name=u"Раскольников").first_name, u'Иван')

    ##### DELETE tests
    def test_delete_unauthenticated(self):
        self.assertHttpUnauthorized(self.api_client.delete("/api/v1/party/person/"))

    def test_delete_detail_unauthenticated_json(self):
        self.assertHttpUnauthorized(self.api_client.delete("/api/v1/party/person/{0}/".format(self.record.tax_no)))

    def test_delete(self):
        self.assertHttpAccepted(self.api_client.delete("/api/v1/party/person/",
                                authentication=self.get_superuser_credentials()))

    def test_delete_detail_json(self):
        record = Person.objects.create(first_name=u'Иван', middle_name=u'Петрович', last_name=u'Лохов',
                                       tax_no=12346578901)
        self.assertEqual(Person.objects.count(), 2)
        self.assertHttpAccepted(self.api_client.delete("/api/v1/party/person/{0}/".format(record.tax_no),
                                authentication=self.get_superuser_credentials()))
        self.assertEqual(Person.objects.count(), 1)


class LegalResourceTest(ResourceTestCase):
    def setUp(self):
        super(LegalResourceTest, self).setUp()

        self.cred = {
            'user': {'username': 'ivan.fomichev', 'password': '12345789', 'email': 'fomka@mail.ru', },
            'superuser': {'username': 'igor.kovalenko', 'password': 'j0ker', 'email': 'kovalenko@sb-soft.biz', },
        }
        User.objects.create_user(
            self.cred['user']['username'], self.cred['user']['email'], self.cred['user']['password'])
        su = User.objects.create_user(
            self.cred['superuser']['username'], self.cred['superuser']['email'], self.cred['superuser']['password'])
        su.is_superuser = True
        su.save()

        self.record = Legal.objects.create(name=u'ИП Коваленко И. С.', reg_no=310253815900038)

    def get_user_credentials(self):
        return self.create_basic(username=self.cred['user']['username'], password=self.cred['user']['password'])

    def get_superuser_credentials(self):
        return self.create_basic(username=self.cred['superuser']['username'],
                                 password=self.cred['superuser']['password'])

    ##### GET tests
    def test_get_unauthorized(self):
        self.assertHttpUnauthorized(self.api_client.get("/api/v1/party/legal/", format='json'))

    def test_get_detail_unauthorized(self):
        self.assertHttpUnauthorized(self.api_client.get("/api/v1/party/legal/{0}/".format(310253815900038),
                                                        format='json'))

    def test_get_list_json(self):
        resp = self.api_client.get('/api/v1/party/legal/', format='json',
                                   authentication=self.get_superuser_credentials())

        self.assertValidJSONResponse(resp)

        self.assertEqual(len(self.deserialize(resp)['objects']), 1)

        record = {
            'party': [],
            'name': u'ИП Коваленко И. С.',
            'reg_no': 310253815900038,
            'id': self.record.id,
            'resource_uri': "/api/v1/party/legal/{0}/".format(310253815900038),
        }
        self.assertEqual(self.deserialize(resp)['objects'].count(record), 1)

    def test_get_detail_json(self):
        resp = self.api_client.get("/api/v1/party/legal/{0}/".format(self.record.reg_no), format='json',
                                   authentication=self.get_superuser_credentials())

        self.assertValidJSONResponse(resp)

        self.assertKeys(self.deserialize(resp), ['id', 'name', 'reg_no', 'party', 'resource_uri', ])
        self.assertEqual(self.deserialize(resp)['id'], self.record.id)

    ##### POST tests
    def test_post_unauthenticated(self):
        self.assertHttpUnauthorized(self.api_client.post("/api/v1/party/legal/"))

    def test_post_detail_unauthenticated(self):
        self.assertHttpUnauthorized(self.api_client.post("/api/v1/party/legal/{0}/".format(self.record.reg_no)))

    def test_post_json(self):
        record = {'name': u"SB-Soft", 'reg_no': 710252815900439, }

        self.assertHttpCreated(self.api_client.post("/api/v1/party/legal/", data=record, format='json',
                                                    authentication=self.get_superuser_credentials()))
        self.assertEqual(len(Legal.objects.filter(reg_no=710252815900439)), 1)

    def test_post_detail_json(self):
        self.assertHttpNotImplemented(self.api_client.post("/api/v1/party/legal/{0}/".format(self.record.reg_no),
                                      authentication=self.get_superuser_credentials()))

    ##### PUT tests
    def test_put_unauthenticated(self):
        self.assertHttpUnauthorized(self.api_client.put("/api/v1/party/legal/".format(self.record.reg_no)))

    def test_put_detail_unauthenticated(self):
        self.assertHttpUnauthorized(self.api_client.put("/api/v1/party/legal/{0}/".format(self.record.reg_no)))

    def test_put_json(self):
        self.assertHttpBadRequest(self.api_client.put("/api/v1/party/legal/",
                                                      authentication=self.get_superuser_credentials()))

    def test_put_detail_json(self):
        record = Legal.objects.create(name=u'SB-Soft', reg_no=710252815900439L)
        new_record = {'name': u"SB-Soft Ltd", 'reg_no': 710252815900439L, }

        self.assertHttpAccepted(self.api_client.put("/api/v1/party/legal/{0}/".format(record.reg_no),
                                                    data=new_record, format='json',
                                                    authentication=self.get_superuser_credentials()))
        self.assertEqual(len(Legal.objects.filter(name=new_record['name'])), 1)

    ##### PATCH tests
    def test_patch_unauthenticated_json(self):
        self.assertHttpUnauthorized(self.api_client.patch("/api/v1/party/legal/", data={}))

    def test_patch_detail_unauthenticated_json(self):
        self.assertHttpUnauthorized(self.api_client.patch("/api/v1/party/legal/{0}/".format(self.record.id), data={}))

    def test_patch_json(self):
        self.assertHttpBadRequest(self.api_client.put("/api/v1/party/legal/", data={},
                                                      authentication=self.get_superuser_credentials()))

    def test_patch_detail_json(self):
        record = Legal.objects.create(name=u'SB-Soft', reg_no=710252815900439)
        new_record = {'name': u"SB-Soft Ltd", }

        self.assertHttpAccepted(self.api_client.patch("/api/v1/party/legal/{0}/".format(record.reg_no),
                                                      data=new_record, format='json',
                                                      authentication=self.get_superuser_credentials()))
        self.assertEqual(len(Legal.objects.filter(name=new_record['name'])), 1)

    ##### DELETE tests
    def test_delete_unauthenticated(self):
        self.assertHttpUnauthorized(self.api_client.delete("/api/v1/party/legal/"))

    def test_delete_detail_unauthenticated_json(self):
        self.assertHttpUnauthorized(self.api_client.delete("/api/v1/party/legal/{0}/".format(self.record.id)))

    def test_delete(self):
        self.assertHttpAccepted(self.api_client.delete("/api/v1/party/legal/",
                                authentication=self.get_superuser_credentials()))

    def test_delete_detail_json(self):
        record = Legal.objects.create(name=u'SB-Soft', reg_no=710252815900439)
        self.assertEqual(Legal.objects.count(), 2)
        self.assertHttpAccepted(self.api_client.delete("/api/v1/party/legal/{0}/".format(record.reg_no),
                                authentication=self.get_superuser_credentials()))
        self.assertEqual(Legal.objects.count(), 1)


class PartyResourceTest(ResourceTestCase):
    def setUp(self):
        super(PartyResourceTest, self).setUp()
        self.maxDiff = None

        self.cred = {
            'user': {'username': 'ivan.fomichev', 'password': '12345789', 'email': 'fomka@mail.ru', },
            'superuser': {'username': 'igor.kovalenko', 'password': 'j0ker', 'email': 'kovalenko@sb-soft.biz', },
        }
        User.objects.create_user(
            self.cred['user']['username'], self.cred['user']['email'], self.cred['user']['password'])
        su = User.objects.create_user(
            self.cred['superuser']['username'], self.cred['superuser']['email'], self.cred['superuser']['password'])
        su.is_superuser = True
        su.save()

        self.party = Party.objects.create(name=u'ИП Коваленко И. С.')

        self.person = Person.objects.create(first_name=u'Игорь', middle_name=u'Станиславович', last_name=u'Коваленко',
                                            tax_no=12345678901, )
        self.person.party.add(self.party)

        self.legal = Legal.objects.create(name=u'ИП Коваленко И. С.', reg_no=310253815900038, )
        self.legal.party.add(self.party)

        self.country = Country.objects.create(name=u'РОССИЯ', full_name=u'Российская Федерация', symbol=u'RU', code=643)
        self.address = Address.objects.create(country=self.country, city=u'Владивосток', street=u'Днепровский пер.',
                                              house=u'4', flat=u'934', postal_index=u'690062', party=self.party)
        self.link_channel = LinkChannel.objects.create(name=u'Mobile', value=u'+79147906645', party=self.party)

    def get_user_credentials(self):
        return self.create_basic(username=self.cred['user']['username'], password=self.cred['user']['password'])

    def get_superuser_credentials(self):
        return self.create_basic(username=self.cred['superuser']['username'],
                                 password=self.cred['superuser']['password'])

    ##### GET tests
    def test_get_unauthorized(self):
        self.assertHttpUnauthorized(self.api_client.get("/api/v1/party/party/", format='json'))

    def test_get_detail_unauthorized(self):
        self.assertHttpUnauthorized(self.api_client.get("/api/v1/party/party/{0}/".format(self.party.id),
                                                        format='json'))

    def test_get_list_json(self):
        resp = self.api_client.get('/api/v1/party/party/', format='json',
                                   authentication=self.get_superuser_credentials())
        self.assertValidJSONResponse(resp)

        self.assertEqual(len(self.deserialize(resp)['objects']), 1)

        record = {
            u'id': self.party.id,
            u'name': unicode(self.party.name),
            u'person': [{
                u'party': [
                    u"/api/v1/party/party/{0}/".format(self.party.id),
                ],
                u'id': self.person.id,
                u'first_name': unicode(self.person.first_name),
                u'last_name': unicode(self.person.last_name),
                u'middle_name': unicode(self.person.middle_name),
                u'tax_no': self.person.tax_no,
                u'resource_uri': u"/api/v1/party/person/{0}/".format(self.person.tax_no),
            }],
            u'legal': [{
                u'party': [
                    u"/api/v1/party/party/{0}/".format(self.party.id),
                ],
                u'name': unicode(self.legal.name),
                u'reg_no': self.legal.reg_no,
                u'id': self.legal.id,
                u'resource_uri': u"/api/v1/party/legal/{0}/".format(self.legal.reg_no),
            }],
            u'addresses': [
                {
                    u"id": self.address.id,
                    u"country": {
                        u"code": self.country.code,
                        u"full_name": unicode(self.country.full_name),
                        u"id": self.country.id,
                        u"name": unicode(self.country.name),
                        u"resource_uri": u"/api/v1/country/{0}/".format(self.country.code),
                        u"symbol": unicode(self.country.symbol)
                    },
                    u"postal_index": unicode(self.address.postal_index),
                    u"city": unicode(self.address.city),
                    u"district": self.address.district,
                    u"street": unicode(self.address.street),
                    u"house": unicode(self.address.house),
                    u"flat": unicode(self.address.flat),
                    u"party": u"/api/v1/party/party/{0}/".format(self.party.id),
                    u"resource_uri": u"/api/v1/party/address/{0}/".format(self.address.id),
                }
            ],
            u'link_channels': [
                {
                    u'id': self.link_channel.id,
                    u'name': unicode(self.link_channel.name),
                    u'value': unicode(self.link_channel.value),
                    u'party': u"/api/v1/party/party/{0}/".format(self.party.id),
                    u'resource_uri': u"/api/v1/party/link_channel/{0}/".format(self.link_channel.id),
                }
            ],
            u'resource_uri': u"/api/v1/party/party/{0}/".format(self.party.id),
        }
        self.assertEqual(self.deserialize(resp)['objects'].count(record), 1)

    def test_get_detail_json(self):
        resp = self.api_client.get("/api/v1/party/party/{0}/".format(self.party.id), format='json',
                                   authentication=self.get_superuser_credentials())
        self.assertValidJSONResponse(resp)

        self.assertKeys(self.deserialize(resp), ['id', 'name', 'person', 'legal', 'resource_uri', 'addresses',
                                                 'link_channels'])
        self.assertEqual(self.deserialize(resp)['id'], self.party.id)

    ##### POST tests
    def test_post_unauthenticated(self):
        self.assertHttpUnauthorized(self.api_client.post("/api/v1/party/party/"))

    def test_post_detail_unauthenticated(self):
        self.assertHttpUnauthorized(self.api_client.post("/api/v1/party/party/{0}/".format(self.party.id)))

    def test_post_legal_party_json(self):
        new_legal_party = {
            u'name': u'SB-Soft',
            u'legal': [{
                u'name': u'SB-Soft',
                u'reg_no': 123456789012,
            }],
            u'addresses': [
                {
                    u"country": 643,
                    u"postal_index": u'690062',
                    u"city": u'Владивосток',
                    u"district": None,
                    u"street": u'Днепровский пер.',
                    u"house": u'4',
                    u"flat": u'934',
                }
            ],
            u'link_channels': [
                {
                    u'name': u'Phone',
                    u'value': u'+79147906645',
                }
            ],
        }

        self.assertHttpCreated(self.api_client.post("/api/v1/party/party/", data=new_legal_party, format='json',
                                                    authentication=self.get_superuser_credentials()))
        p = Party.objects.filter(name=u"SB-Soft")
        self.assertEqual(len(p), 1)
        self.assertEqual(len(p[0].legal_set.filter(name=u"SB-Soft")), 1)
        self.assertEqual(len(p[0].address_set.all()), 1)
        self.assertEqual(p[0].address_set.all()[0].country.code, 643)
        self.assertEqual(len(p[0].linkchannel_set.all()), 1)
        self.assertEqual(p[0].linkchannel_set.all()[0].value, u'+79147906645')

    def test_post_detail_json(self):
        self.assertHttpNotImplemented(self.api_client.post("/api/v1/party/party/{0}/".format(self.party.id),
                                      authentication=self.get_superuser_credentials()))

    def test_post_person_party_json(self):
        new_legal_party = {
            u'name': u'Коваленко И. С.',
            u'person': [{
                u'first_name': u'Игорь',
                u'middle_name': u'Станиславович',
                u'second_name': u'Коваленко',
                u'tax_no': 123456789012,
            }],
            u'addresses': [
                {
                    u"country": 643,
                    u"postal_index": u'690062',
                    u"city": u'Владивосток',
                    u"district": None,
                    u"street": u'Днепровский пер.',
                    u"house": u'4',
                    u"flat": u'934',
                }
            ],
            u'link_channels': [
                {
                    u'name': u'Phone',
                    u'value': u'+79147906645',
                }
            ],
        }

        self.assertHttpCreated(self.api_client.post("/api/v1/party/party/", data=new_legal_party, format='json',
                                                    authentication=self.get_superuser_credentials()))
        p = Party.objects.filter(name=u"SB-Soft")
        self.assertEqual(len(p), 1)
        self.assertEqual(len(p[0].legal_set.filter(name=u"SB-Soft")), 1)
        self.assertEqual(len(p[0].address_set.all()), 1)
        self.assertEqual(p[0].address_set.all()[0].country.code, 643)
        self.assertEqual(len(p[0].linkchannel_set.all()), 1)
        self.assertEqual(p[0].linkchannel_set.all()[0].value, u'+79147906645')

    def test_post_detail_json(self):
        self.assertHttpNotImplemented(self.api_client.post("/api/v1/party/party/{0}/".format(self.party.id),
                                      authentication=self.get_superuser_credentials()))

#     # ##### PUT tests
#     # def test_put_unauthenticated(self):
#     #     self.assertHttpUnauthorized(self.api_client.put("/api/v1/party/person/".format(self.record.tax_no)))
#     #
#     # def test_put_detail_unauthenticated(self):
#     #     self.assertHttpUnauthorized(self.api_client.put("/api/v1/party/person/{0}/".format(self.record.tax_no)))
#     #
#     # def test_put_json(self):
#     #     self.assertHttpBadRequest(self.api_client.put("/api/v1/party/person/",
#     #                                                   authentication=self.get_superuser_credentials()))
#     #
#     # def test_put_detail_json(self):
#     #     record = Person.objects.create(first_name=u'Иван', middle_name=u'Петрович', last_name=u'Лохов',
#     #                                    tax_no=12346578901)
#     #     new_record = {'first_name': u"Иван", 'last_name': u"Раскольников", 'middle_name': u"Петрович",
#     #                   'tax_no': 12346578901}
#     #
#     #     self.assertHttpAccepted(self.api_client.put("/api/v1/party/person/{0}/".format(record.tax_no),
#     #                                                 data=new_record, format='json',
#     #                                                 authentication=self.get_superuser_credentials()))
#     #     self.assertEqual(len(Person.objects.filter(last_name=u"Раскольников")), 1)
#     #
#     # ##### PATCH tests
#     # def test_patch_unauthenticated_json(self):
#     #     self.assertHttpUnauthorized(self.api_client.patch("/api/v1/party/person/", data={}))
#     #
#     # def test_patch_detail_unauthenticated_json(self):
#     #     self.assertHttpUnauthorized(self.api_client.patch("/api/v1/party/person/{0}/".format(self.record.tax_no),
#     #                                                       data={}))
#     #
#     # def test_patch_json(self):
#     #     self.assertHttpBadRequest(self.api_client.put("/api/v1/party/person/", data={},
#     #                                                   authentication=self.get_superuser_credentials()))
#     #
#     # def test_patch_detail_json(self):
#     #     record = Person.objects.create(first_name=u'Иван', middle_name=u'Петрович', last_name=u'Лохов',
#     #                                    tax_no=12346578901)
#     #     new_record = {'last_name': u"Раскольников", }
#     #
#     #     self.assertHttpAccepted(self.api_client.patch("/api/v1/party/person/{0}/".format(record.tax_no),
#     #                                                   data=new_record, format='json',
#     #                                                   authentication=self.get_superuser_credentials()))
#     #     self.assertEqual(Person.objects.get(last_name=u"Раскольников").first_name, u'Иван')
#     #
#     # ##### DELETE tests
#     # def test_delete_unauthenticated(self):
#     #     self.assertHttpUnauthorized(self.api_client.delete("/api/v1/party/person/"))
#     #
#     # def test_delete_detail_unauthenticated_json(self):
#     #     self.assertHttpUnauthorized(self.api_client.delete("/api/v1/party/person/{0}/".format(self.record.tax_no)))
#     #
#     # def test_delete(self):
#     #     self.assertHttpAccepted(self.api_client.delete("/api/v1/party/person/",
#     #                             authentication=self.get_superuser_credentials()))
#     #
#     # def test_delete_detail_json(self):
#     #     record = Person.objects.create(first_name=u'Иван', middle_name=u'Петрович', last_name=u'Лохов',
#     #                                    tax_no=12346578901)
#     #     self.assertEqual(Person.objects.count(), 2)
#     #     self.assertHttpAccepted(self.api_client.delete("/api/v1/party/person/{0}/".format(record.tax_no),
#     #                             authentication=self.get_superuser_credentials()))
#     #     self.assertEqual(Person.objects.count(), 1)
#     #
#     #
